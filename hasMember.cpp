/*
The MIT License (MIT)

Copyright (c) 2014 Paweł Turkowski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "hasMember.h"
#include "staticIf.h"
#include <iostream>
#include <type_traits>
#include <cassert>
#include <functional>

struct A{
	int foo;
};
struct B{
  void bar();
};
struct C{
  int foo() const {
    std::cout << "meanwhile 'C foo' ...\n";
    return 42;
  }
};



template <class T, class R>
R callOrGet(const T& t0, R T::*ptr){
  return t0.*ptr;
}

template <class T, class R, class... Args>
R callOrGet(const T& t0, R (T::*ptr)(Args...) const, Args&&... args){
  return (t0.*ptr)(std::forward<Args>(args)...);
}


template <class T>
int getFoo(const T& t0, const char* name){
  constexpr auto L1 = MEMBER_CHECKER_XOR(foo);
  constexpr auto L2 = MEMBER_CHECKER_XOR(foo());
  return static_if_<hasMember(t0, L1) || hasMember(t0, L2)>(
    [=](auto t){
      std::cout << name << " have foo\n";
      return callOrGet(t, &T::foo);
    },
    [=](auto t){
      std::cout << name <<" doesn't have foo\n";
      return 0;
    },
    t0
  );
}


int main() {
  
  #define print(x) std::cout<< #x " = " << x << '\n';
  
  print(hasMember<A>(MEMBER_CHECKER_XOR(foo)));
  print(hasMember<A>(MEMBER_CHECKER_XOR(asd)));
  print(hasMember<A>(MEMBER_CHECKER_XOR(bar)));
  static_assert(hasMember<A>(MEMBER_CHECKER_XOR(foo)) == 1, "");
  static_assert(hasMember<A>(MEMBER_CHECKER_XOR(asd)) == 0, "");
  static_assert(hasMember<A>(MEMBER_CHECKER_XOR(bar)) == 0, "");
  
  print(hasMember<B>(MEMBER_CHECKER_XOR(foo)));
  print(hasMember<B>(MEMBER_CHECKER_XOR(asd)));
  print(hasMember<B>(MEMBER_CHECKER_XOR(bar)));
  static_assert(hasMember<B>(MEMBER_CHECKER_XOR(foo)) == 0, "");
  static_assert(hasMember<B>(MEMBER_CHECKER_XOR(asd)) == 0, "");

  static constexpr auto* checkerFoo = false ? address([](auto a, decltype(a.foo)*){}) : nullptr;
  static_assert(hasMember<B>(checkerFoo) == 0, "");
  static_assert(hasMember<B>(address([](auto a, decltype(a.foo)*){})) == 0, "");
  
  static_assert(hasMember<B>(MEMBER_CHECKER_XOR(bar())) == 1, "");
  static_assert(hasMember<B>(MEMBER_CHECKER_XOR(bar)) == 0, "");
  static_assert(hasMember<B>(MEMBER_CHECKER(bar)) == 1, "");
  
  static_assert(hasMember<std::is_class<B>>(MEMBER_CHECKER_XOR(value)) == 1, "");
  print(hasMember<std::is_class<B>>(MEMBER_CHECKER_XOR(value)));
  
  #undef print
  
  A a; a.foo = 1;
  B b;
  C c;
  
  assert(getFoo(a, "A") == 1);
  assert(getFoo(b, "B") == 0);
  assert(getFoo(c, "C") == 42);
  
  return 0;
}
