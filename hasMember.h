/*
The MIT License (MIT)

Copyright (c) 2014 Paweł Turkowski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <utility>
#include <cstdint>

template <class T, class F>
struct MemberTester{
  using no_t = int8_t;
  using yes_t = int16_t;
  
  // Easy acces to type returned by lambda
  template<class T1, class F1>
  using Decl = decltype(std::declval<F1>()(std::declval<T1>(), nullptr));
  
  template<class T1, class F1>
  static yes_t test(Decl<T1,F1>* P); 
  
  template<class T1, class F1>
  static no_t test(...); 
 
  constexpr static int value = sizeof(test<T, F>(0)) == sizeof(yes_t);

  constexpr operator int() { return value; }
};
   
template <class T, class F>
constexpr int hasMember(F* f){
  return MemberTester<T, F>{};
}

template <class T, class F>
constexpr int hasMember(const T&, F* f){
  return MemberTester<T, F>{};
}

template <class T, class F>
constexpr int hasMember(){
  return MemberTester<T, F>{};
}

template<typename T>
constexpr inline
auto address(T&& t) -> typename std::remove_reference<T>::type*
{
  return &t;
}

//! Checks if type have member variable OR member function with given name
#define MEMBER_CHECKER(field) false ? address([](auto a, decltype(&std::remove_reference<decltype(a)>::type::field)){}) : nullptr

//! Checks if type have member variable XOR member function with given name and arguments
#define MEMBER_CHECKER_XOR(name) false ? address([](auto a, decltype(a.name)*){}) : nullptr
