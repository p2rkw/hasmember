/*
The MIT License (MIT)

Copyright (c) 2014 Paweł Turkowski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

template<int Cond, class Branch_>
struct StaticIF_{
  using Branch = Branch_;
  
  Branch & branch;
  
  StaticIF_(Branch& branch) : branch(branch){}
};

template<int Cond, class True, class False>
struct StaticIF : StaticIF_<Cond, True>{
  using Base = StaticIF_<Cond, True>;
  StaticIF(True& trueBranch, False& ) : Base(trueBranch){}
};

template<class True, class False>
struct StaticIF<0, True, False> : StaticIF_<0, False>{
  using Base = StaticIF_<0, False>;
  StaticIF(True& , False& falseBranch) : Base(falseBranch){}
};


template<int Cond, class True, class False, class... Captures>
auto static_if_(True&& t, False&& f, Captures&& ... captures){
  StaticIF<Cond, True, False> sif(t, f);
  return sif.branch(std::forward<Captures>(captures)...);
}

